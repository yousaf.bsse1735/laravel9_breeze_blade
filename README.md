## Steps for Installation

- [Laravel Installation](https://laravel.com/docs/9.x/installation#your-first-laravel-project)
```
laravel new laravel9_breeze_blade
```

- [Git initiation](https://docs.gitlab.com/ee/tutorials/make_your_first_git_commit.html#create-a-sample-project)
```
git init --initial-branch=main
git remote add origin https://gitlab.com/yousaf.bsse1735/laravel9_breeze_blade.git
git add .
git commit -m "Initial commit"
git push -u origin main
```

- [Create DB](https://hevodata.com/learn/xampp-mysql)

- [Edit host file](https://www.howtogeek.com/27350/beginner-geek-how-to-edit-your-hosts-file/)
in folder C:\Windows\System32\drivers\etc
open file hosts
and add following
```
127.0.0.1 laravel9_breeze_blade.test
```

- [configure virtual host](https://www.cloudways.com/blog/configure-virtual-host-on-windows-10-for-wordpress/)
in folder C:\xampp\apache\conf\extra
open file httpd-vhosts.conf
and add following
```
<VirtualHost *:80>
DocumentRoot "c:/xampp/htdocs/laravel9_breeze_blade/public"
ServerName laravel9_breeze_blade.test
<Directory "c:/xampp/htdocs/laravel9_breeze_blade/public">
</Directory>
</VirtualHost>
```
## open browser and Run

```
http://laravel9_breeze_blade.test
```

## Install breeze with Blade

- [Install breeze with Blade](https://laravel.com/docs/9.x/starter-kits#breeze-and-blade)

```
composer require laravel/breeze --dev

php artisan breeze:install
 
php artisan migrate
npm install
npm run dev
```

### Login with google

- [Create Project](https://console.cloud.google.com)

Add Credentials OAuth Client ID

Authorised JavaScript origins
http://aravel9vue3blade.com

Add Authorised redirect URIs
http://aravel9vue3blade.com/auth/google/callback

Fill OAuth Consent Screen
User Type External
Add Test Users

- [Add socialite package](https://laravel.com/docs/9.x/socialite)
