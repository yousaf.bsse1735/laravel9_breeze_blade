@props([
        'padding' => 'p-4',
        'size' => 'text-base',
        'textColor' => 'text-cyan-800',
        'bgColor' => 'bg-gradient-to-br from-green-300 via-cyan-300 to-blue-300',
    ])

@php
    $containerClasses = $padding . ' ' . $bgColor . ' grid justify-items-center content-center rounded-full shadow-lg mb-4';
    $paragraphClasses = $size . ' ' . $textColor;
@endphp

<div {{ $attributes->merge(['class' => $containerClasses]) }}>
    <p  {{ $attributes->merge(['class' => $paragraphClasses]) }}>{{ $slot }}</p>
</div>